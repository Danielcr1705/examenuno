﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExamenUno.Clases;

namespace ExamenUno.Modelos
{
    class Carro : Vehiculo
    {
        private TypeOfVehicle.TypeOfAutomobile typeOfAutomobile;
        private int numberOfDoors;

        public Carro(String registration, int model, TypeOfVehicle.BrandOfVehicle brand, TypeOfVehicle.ColorOfVehicle color, int passengerCapacity, double fiscalValue, int doors, TypeOfVehicle.TypeOfAutomobile automobile)
            : base(registration, model, brand,color, passengerCapacity, fiscalValue )
        {
            this.typeOfAutomobile = automobile;
            this.numberOfDoors = doors;
        }


        public override double CalculateMandatoryInsurance()
        {
            double MT = 0;
            double VF = this._fiscalValue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = CalculateTaxForCar();

            switch (this.typeOfAutomobile)
            {
                case TypeOfVehicle.TypeOfAutomobile.SUV:
                    MT = (VF * 10 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfAutomobile.Sedan:
                    MT = (VF * 5 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfAutomobile.Haatchback:
                    MT = (VF * 7 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfAutomobile.PickUp:
                    MT = (VF * 3 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfAutomobile.Coupe:
                    MT = (VF * 5 / 100 / AA) + MC;
                    break;
                default:
                    break;
            }

            return MT;
        }

        private double CalculateTaxForCar() {
            return (_fiscalValue / 50) * 1 * numberOfDoors;
        }

        //Método get y set.
        public int NumberOfDoors
        {
            get { return numberOfDoors; }
            set { numberOfDoors = value; }
        }
    }
}
