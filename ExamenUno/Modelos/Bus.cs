﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExamenUno.Clases;

namespace ExamenUno.Modelos
{
    class Bus : Vehiculo
    {
        private TypeOfVehicle.TypeOfService typeOfService;
        private int seatingCapacity;

        public Bus(String registration, int model, TypeOfVehicle.BrandOfVehicle brand, TypeOfVehicle.ColorOfVehicle color, int passengerCapacity, double fiscalValue, int capacity, TypeOfVehicle.TypeOfService service)
            : base(registration, model, brand,color, passengerCapacity, fiscalValue )
        {
        this.seatingCapacity = capacity;
        this.typeOfService = service;
        }

        public override double CalculateMandatoryInsurance()
        {
            double MT = 0;
            double VF = this._fiscalValue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = CalculateTaxForBus();

            switch (this.typeOfService)
            {
                case TypeOfVehicle.TypeOfService.Tourism:
                    MT = (VF * 10 / 100) * (AA * 5 / 100) + MC;
                    break;
                case TypeOfVehicle.TypeOfService.PublicTransport:
                    MT = (VF * 5 / 100) * (AA * 5 / 100) + MC;
                    break;
                case TypeOfVehicle.TypeOfService.TransportingOfStudents:
                    MT = (VF * 3 / 100) * (AA * 5 / 100) + MC;
                    break;
                default:
                    break;
            }
            return MT;
        }

        private double CalculateTaxForBus()
        {
            return (_fiscalValue / 150) * 1 * seatingCapacity;
        }

        //Métodos gets y sets.
        public int SeatingCapacity
        {
         get { return seatingCapacity; }
        set { seatingCapacity = value; }
        }
    }
}
