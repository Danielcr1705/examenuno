﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExamenUno.Clases;

namespace ExamenUno.Modelos
{
    class Truck : Vehiculo
    {
        private TypeOfVehicle.TypeOfTruck typeOfTruck;
        private int numberOfAxles;

        public Truck(String registration, int model, TypeOfVehicle.BrandOfVehicle brand, TypeOfVehicle.ColorOfVehicle color, int passengerCapacity, double fiscalValue, int number, TypeOfVehicle.TypeOfTruck truck)
            : base(registration, model, brand,color, passengerCapacity, fiscalValue )
        {
        this.typeOfTruck = truck;
        this.numberOfAxles = number;
        }

        public override double CalculateMandatoryInsurance()
        {
            double MT = 0;
            double VF = this._fiscalValue;
            int AA = Math.Max((DateTime.Today.Year - this._model),1);
            double MC = CalculateTaxForTruck();

            switch (this.typeOfTruck)
            {
                case TypeOfVehicle.TypeOfTruck.BallastTractor:
                    MT = (VF * 10 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.ConcreteTransportTruck:
                    MT = (VF * 5 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.CraneTruck:
                    MT = (VF * 20 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.DumpTruck:
                    MT = (VF * 5 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.GarbageTruck:
                    MT = (VF * 2 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.LogCarrier:
                    MT = (VF * 12 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.RefrigeratorTruck:
                    MT = (VF * 15 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.SemiTrailer:
                    MT = (VF * 7 / 100 / AA) + MC;
                    break;
                case TypeOfVehicle.TypeOfTruck.TankTruck:
                    MT = (VF * 10 / 100 / AA) + MC;
                    break;
                default:
                    break;
            }
            return MT;
        }

        //Método get y set.
        public int NumberOfAxles
        {
            get { return numberOfAxles; }
            set { numberOfAxles = value; }
        }

        private double CalculateTaxForTruck() 
        {
            return (_fiscalValue / 50) * 1 / 100 * numberOfAxles;
        }
    }
}
