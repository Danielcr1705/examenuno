﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenUno.Clases
{
    class Vehiculo
    {
        protected String _registration;
        protected int _model;
        protected TypeOfVehicle.BrandOfVehicle _brand;
        protected TypeOfVehicle.ColorOfVehicle _color;
        protected int _passengerCapacity;
        protected double _fiscalValue;

        public Vehiculo(String registration, int model, TypeOfVehicle.BrandOfVehicle brand, TypeOfVehicle.ColorOfVehicle color, int passengerCapacity, double fiscalValue) 
        {
            this._registration = registration;
            this._model = model;
            this._brand = brand;
            this._color = color;
            this._passengerCapacity = passengerCapacity;
            this._fiscalValue = fiscalValue;      
        }

        public abstract double CalculateMandatoryInsurance();

    }
}
