﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenUno.Clases
{
    class TypeOfVehicle
    {
        public enum BrandOfVehicle
        {
            Toyota,
            Hyundai,
            Honda,
            Mitsubishi,
            MercedesBenz,
            Volkswagen,
            Suzuki,
            Peugeot,
            Kia,
            Mazda,
            Chevrolet,
            Scania,
            Mack,
            Volvo,
        }

        public enum ColorOfVehicle
        {
            Red,
            Green,
            Blue,
            Black,
            White,
            Aero,
            AmaranthPink,
            CanaryYellow,
            Bronze,
            FuchsiaPink,
        }

        public enum TypeOfAutomobile 
        {
        
            SUV,
            Sedan,
            Haatchback,
            PickUp,
            Coupe,
        }

        public enum TypeOfService 
        {
        
            Tourism,
            PublicTransport,
            TransportingOfStudents,
        }

        public enum TypeOfTruck 
        {
        
            BallastTractor,
            ConcreteTransportTruck,
            CraneTruck,
            DumpTruck,
            GarbageTruck,
            LogCarrier,
            RefrigeratorTruck,
            SemiTrailer,
            TankTruck,
        }

        public enum TypeOfCar 
        {   
            Car,
            Bus,
            Truck,
        }

    }
}
