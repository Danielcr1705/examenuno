﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenUno.Clases;

namespace ExamenUno.Views
{
    public partial class EmployeeForm : Form
    {
        Dictionary<string, TypeOfVehicle.TypeOfCar> dictionary1;
        Dictionary<string, TypeOfVehicle.BrandOfVehicle> dictionary2;
        Dictionary<string, TypeOfVehicle.ColorOfVehicle> dictionary3;
        Dictionary<string, TypeOfVehicle.TypeOfAutomobile> dictionary4;
        Dictionary<string, TypeOfVehicle.TypeOfService> dictionary5;
        Dictionary<string, TypeOfVehicle.TypeOfTruck> dictionary6;
        

        public EmployeeForm()
        {
            InitializeComponent();

            //Diccionario para carro
            dictionary1 = new Dictionary<string,TypeOfVehicle.TypeOfCar>();
            dictionary1.Add("Carro",TypeOfVehicle.TypeOfCar.Car);
            dictionary1.Add("Bus", TypeOfVehicle.TypeOfCar.Bus);
            dictionary1.Add("Camión", TypeOfVehicle.TypeOfCar.Truck);

            this.gbAutobus.Enabled = false;
            this.gbAutomovil.Enabled = false;
            this.gbCamion.Enabled = false;
            

            this.cmbTypeOfVehicle.DisplayMember = "Key";
            this.cmbTypeOfVehicle.ValueMember = "Value";
            this.cmbTypeOfVehicle.DataSource = new BindingSource(dictionary1, null);
            this.cmbTypeOfVehicle.DataSource = new BindingSource(dictionary2, null);
            this.cmbTypeOfVehicle.DataSource = new BindingSource(dictionary3, null);
            this.cmbTypeOfVehicle.DataSource = new BindingSource(dictionary4, null);
            this.cmbTypeOfVehicle.DataSource = new BindingSource(dictionary5, null);
            this.cmbTypeOfVehicle.DataSource = new BindingSource(dictionary6, null);
            this.cmbTypeOfVehicle.DropDownStyle = ComboBoxStyle.DropDownList;

            //Diccionario para brand
            dictionary2 = new Dictionary<string, TypeOfVehicle.BrandOfVehicle>();
            dictionary2.Add("Chevrolet", TypeOfVehicle.BrandOfVehicle.Chevrolet);
            dictionary2.Add("Honda", TypeOfVehicle.BrandOfVehicle.Honda);
            dictionary2.Add("Hyundai", TypeOfVehicle.BrandOfVehicle.Hyundai);
            dictionary2.Add("Kia", TypeOfVehicle.BrandOfVehicle.Kia);
            dictionary2.Add("Mack", TypeOfVehicle.BrandOfVehicle.Mack);
            dictionary2.Add("Mazda", TypeOfVehicle.BrandOfVehicle.Mazda);
            dictionary2.Add("MercedesBenz", TypeOfVehicle.BrandOfVehicle.MercedesBenz);
            dictionary2.Add("Mitsubishi", TypeOfVehicle.BrandOfVehicle.Mitsubishi);
            dictionary2.Add("Peugeot", TypeOfVehicle.BrandOfVehicle.Peugeot);
            dictionary2.Add("Scania", TypeOfVehicle.BrandOfVehicle.Scania);
            dictionary2.Add("Suzuki", TypeOfVehicle.BrandOfVehicle.Suzuki);
            dictionary2.Add("Toyota", TypeOfVehicle.BrandOfVehicle.Toyota);
            dictionary2.Add("Volkswagen", TypeOfVehicle.BrandOfVehicle.Volkswagen);
            dictionary2.Add("Volvo", TypeOfVehicle.BrandOfVehicle.Volvo);

            //Diccionario color
            dictionary3.Add("Aero", TypeOfVehicle.ColorOfVehicle.Aero);
            dictionary3.Add("Amaranth Pink", TypeOfVehicle.ColorOfVehicle.AmaranthPink);
            dictionary3.Add("Black", TypeOfVehicle.ColorOfVehicle.Black);
            dictionary3.Add("Blue", TypeOfVehicle.ColorOfVehicle.Blue);
            dictionary3.Add("Bronze", TypeOfVehicle.ColorOfVehicle.Bronze);
            dictionary3.Add("Canary Yellow", TypeOfVehicle.ColorOfVehicle.CanaryYellow);
            dictionary3.Add("Fuchsia Pink", TypeOfVehicle.ColorOfVehicle.FuchsiaPink);
            dictionary3.Add("Green", TypeOfVehicle.ColorOfVehicle.Green);
            dictionary3.Add("Red", TypeOfVehicle.ColorOfVehicle.Red);
            dictionary3.Add("White", TypeOfVehicle.ColorOfVehicle.White);

            //Diccionario automovil
            dictionary4.Add("Coupe", TypeOfVehicle.TypeOfAutomobile.Coupe);
            dictionary4.Add("Haatchback", TypeOfVehicle.TypeOfAutomobile.Haatchback);
            dictionary4.Add("PickUp", TypeOfVehicle.TypeOfAutomobile.PickUp);
            dictionary4.Add("Sedan", TypeOfVehicle.TypeOfAutomobile.Sedan);
            dictionary4.Add("SUV", TypeOfVehicle.TypeOfAutomobile.SUV);

            //Diccionario para el servicio
            dictionary5.Add("Public Transport", TypeOfVehicle.TypeOfService.PublicTransport);
            dictionary5.Add("Tourism", TypeOfVehicle.TypeOfService.Tourism);
            dictionary5.Add("Transporting of Students", TypeOfVehicle.TypeOfService.TransportingOfStudents);

            //Diccionario para tipo de camion
            dictionary6.Add("Ballast Tractor", TypeOfVehicle.TypeOfTruck.BallastTractor);
            dictionary6.Add("Concrete Transport Truck", TypeOfVehicle.TypeOfTruck.ConcreteTransportTruck);
            dictionary6.Add("CraneTruck", TypeOfVehicle.TypeOfTruck.CraneTruck);
            dictionary6.Add("Dump Truck", TypeOfVehicle.TypeOfTruck.DumpTruck);
            dictionary6.Add("Garbage Truck", TypeOfVehicle.TypeOfTruck.GarbageTruck);
            dictionary6.Add("Log Carrier", TypeOfVehicle.TypeOfTruck.LogCarrier);
            dictionary6.Add("Refrigerator Truck", TypeOfVehicle.TypeOfTruck.RefrigeratorTruck);
            dictionary6.Add("Semi Trailer", TypeOfVehicle.TypeOfTruck.SemiTrailer);
            dictionary6.Add("Tank Truck", TypeOfVehicle.TypeOfTruck.TankTruck);

        }

        private void btCalculate_Click(object sender, EventArgs e)
        {
            
        }

        private void btClear_Click(object sender, EventArgs e)
        {

        }

        private void cmbTypeOfVehicle_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbTypeOfVehicle.SelectedValue != null)
            {
                this.CleanControlsOfGroups();

                
            }
        }

        private void CleanControlsOfGroups()
        {
            //Operador
            this.txtNumeroPuertas.Text = string.Empty;
            
            //Vendedor
            this.txtCantidadAsientos.Text = string.Empty;

            //Administrador
            this.txtNumeroEjes.Text = string.Empty;
        }

        private void EmployeeForm_Load(object sender, EventArgs e)
        {

        }

        private void cmbTypeOfVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lbFormTitle_Click(object sender, EventArgs e)
        {

        }

    
    }
}
