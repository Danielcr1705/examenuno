﻿namespace ExamenUno.Views
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbFormTitle = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.gbAboutEmployee = new System.Windows.Forms.GroupBox();
            this.lblValorFiscal = new System.Windows.Forms.Label();
            this.lblCapacidad = new System.Windows.Forms.Label();
            this.txtValorFiscal = new System.Windows.Forms.TextBox();
            this.txtCapacidad = new System.Windows.Forms.TextBox();
            this.cmbMarca = new System.Windows.Forms.ComboBox();
            this.cmbColor = new System.Windows.Forms.ComboBox();
            this.btCalculate = new System.Windows.Forms.Button();
            this.btClear = new System.Windows.Forms.Button();
            this.gbCamion = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbTipoCamion = new System.Windows.Forms.ComboBox();
            this.txtNumeroEjes = new System.Windows.Forms.TextBox();
            this.lbOvertime = new System.Windows.Forms.Label();
            this.gbAutobus = new System.Windows.Forms.GroupBox();
            this.cmbTipoServicio = new System.Windows.Forms.ComboBox();
            this.txtCantidadAsientos = new System.Windows.Forms.TextBox();
            this.lbCommission = new System.Windows.Forms.Label();
            this.lbAmountOfSales = new System.Windows.Forms.Label();
            this.gbAutomovil = new System.Windows.Forms.GroupBox();
            this.cmbTipoAuto = new System.Windows.Forms.ComboBox();
            this.txtNumeroPuertas = new System.Windows.Forms.TextBox();
            this.lbAmountOfPiece = new System.Windows.Forms.Label();
            this.lbPartsProduced = new System.Windows.Forms.Label();
            this.lblTipoVehiculo = new System.Windows.Forms.Label();
            this.cmbTypeOfVehicle = new System.Windows.Forms.ComboBox();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblMarca = new System.Windows.Forms.Label();
            this.txtAñoModelo = new System.Windows.Forms.TextBox();
            this.txtPlaca = new System.Windows.Forms.TextBox();
            this.lblAñoModelo = new System.Windows.Forms.Label();
            this.lblPlaca = new System.Windows.Forms.Label();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.txtSeguro = new System.Windows.Forms.TextBox();
            this.lbNetEarnings = new System.Windows.Forms.Label();
            this.panelHeader.SuspendLayout();
            this.gbAboutEmployee.SuspendLayout();
            this.gbCamion.SuspendLayout();
            this.gbAutobus.SuspendLayout();
            this.gbAutomovil.SuspendLayout();
            this.gbResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbFormTitle
            // 
            this.lbFormTitle.AutoSize = true;
            this.lbFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFormTitle.Location = new System.Drawing.Point(12, 9);
            this.lbFormTitle.Name = "lbFormTitle";
            this.lbFormTitle.Size = new System.Drawing.Size(546, 39);
            this.lbFormTitle.TabIndex = 0;
            this.lbFormTitle.Text = "Calculadora de Seguro Obligatorio";
            this.lbFormTitle.Click += new System.EventHandler(this.lbFormTitle_Click);
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelHeader.Controls.Add(this.lbFormTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(690, 59);
            this.panelHeader.TabIndex = 0;
            // 
            // gbAboutEmployee
            // 
            this.gbAboutEmployee.Controls.Add(this.lblValorFiscal);
            this.gbAboutEmployee.Controls.Add(this.lblCapacidad);
            this.gbAboutEmployee.Controls.Add(this.txtValorFiscal);
            this.gbAboutEmployee.Controls.Add(this.txtCapacidad);
            this.gbAboutEmployee.Controls.Add(this.cmbMarca);
            this.gbAboutEmployee.Controls.Add(this.cmbColor);
            this.gbAboutEmployee.Controls.Add(this.btCalculate);
            this.gbAboutEmployee.Controls.Add(this.btClear);
            this.gbAboutEmployee.Controls.Add(this.gbCamion);
            this.gbAboutEmployee.Controls.Add(this.gbAutobus);
            this.gbAboutEmployee.Controls.Add(this.gbAutomovil);
            this.gbAboutEmployee.Controls.Add(this.lblTipoVehiculo);
            this.gbAboutEmployee.Controls.Add(this.cmbTypeOfVehicle);
            this.gbAboutEmployee.Controls.Add(this.lblColor);
            this.gbAboutEmployee.Controls.Add(this.lblMarca);
            this.gbAboutEmployee.Controls.Add(this.txtAñoModelo);
            this.gbAboutEmployee.Controls.Add(this.txtPlaca);
            this.gbAboutEmployee.Controls.Add(this.lblAñoModelo);
            this.gbAboutEmployee.Controls.Add(this.lblPlaca);
            this.gbAboutEmployee.Location = new System.Drawing.Point(12, 65);
            this.gbAboutEmployee.Name = "gbAboutEmployee";
            this.gbAboutEmployee.Size = new System.Drawing.Size(666, 319);
            this.gbAboutEmployee.TabIndex = 1;
            this.gbAboutEmployee.TabStop = false;
            this.gbAboutEmployee.Text = "Datos del Vehículo";
            // 
            // lblValorFiscal
            // 
            this.lblValorFiscal.AutoSize = true;
            this.lblValorFiscal.Location = new System.Drawing.Point(18, 187);
            this.lblValorFiscal.Name = "lblValorFiscal";
            this.lblValorFiscal.Size = new System.Drawing.Size(61, 13);
            this.lblValorFiscal.TabIndex = 20;
            this.lblValorFiscal.Text = "Valor Fiscal";
            // 
            // lblCapacidad
            // 
            this.lblCapacidad.AutoSize = true;
            this.lblCapacidad.Location = new System.Drawing.Point(18, 161);
            this.lblCapacidad.Name = "lblCapacidad";
            this.lblCapacidad.Size = new System.Drawing.Size(58, 13);
            this.lblCapacidad.TabIndex = 19;
            this.lblCapacidad.Text = "Capacidad";
            // 
            // txtValorFiscal
            // 
            this.txtValorFiscal.Location = new System.Drawing.Point(120, 187);
            this.txtValorFiscal.Name = "txtValorFiscal";
            this.txtValorFiscal.Size = new System.Drawing.Size(204, 20);
            this.txtValorFiscal.TabIndex = 18;
            // 
            // txtCapacidad
            // 
            this.txtCapacidad.Location = new System.Drawing.Point(120, 161);
            this.txtCapacidad.Name = "txtCapacidad";
            this.txtCapacidad.Size = new System.Drawing.Size(204, 20);
            this.txtCapacidad.TabIndex = 17;
            // 
            // cmbMarca
            // 
            this.cmbMarca.FormattingEnabled = true;
            this.cmbMarca.Location = new System.Drawing.Point(120, 108);
            this.cmbMarca.Name = "cmbMarca";
            this.cmbMarca.Size = new System.Drawing.Size(204, 21);
            this.cmbMarca.TabIndex = 16;
            // 
            // cmbColor
            // 
            this.cmbColor.FormattingEnabled = true;
            this.cmbColor.Location = new System.Drawing.Point(120, 134);
            this.cmbColor.Name = "cmbColor";
            this.cmbColor.Size = new System.Drawing.Size(204, 21);
            this.cmbColor.TabIndex = 15;
            // 
            // btCalculate
            // 
            this.btCalculate.Image = global::ExamenUno.Properties.Resources.dialog_ok_2;
            this.btCalculate.Location = new System.Drawing.Point(107, 239);
            this.btCalculate.Name = "btCalculate";
            this.btCalculate.Size = new System.Drawing.Size(100, 80);
            this.btCalculate.TabIndex = 13;
            this.btCalculate.Text = "Calcular";
            this.btCalculate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btCalculate.UseVisualStyleBackColor = true;
            this.btCalculate.Click += new System.EventHandler(this.btCalculate_Click);
            // 
            // btClear
            // 
            this.btClear.Image = global::ExamenUno.Properties.Resources.edit_clear_3;
            this.btClear.Location = new System.Drawing.Point(224, 239);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(100, 80);
            this.btClear.TabIndex = 14;
            this.btClear.Text = "Limpiar";
            this.btClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // gbCamion
            // 
            this.gbCamion.Controls.Add(this.label8);
            this.gbCamion.Controls.Add(this.cmbTipoCamion);
            this.gbCamion.Controls.Add(this.txtNumeroEjes);
            this.gbCamion.Controls.Add(this.lbOvertime);
            this.gbCamion.Location = new System.Drawing.Point(351, 198);
            this.gbCamion.Name = "gbCamion";
            this.gbCamion.Size = new System.Drawing.Size(299, 84);
            this.gbCamion.TabIndex = 12;
            this.gbCamion.TabStop = false;
            this.gbCamion.Text = "Camión";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Tipo de Camión";
            // 
            // cmbTipoCamion
            // 
            this.cmbTipoCamion.FormattingEnabled = true;
            this.cmbTipoCamion.Location = new System.Drawing.Point(177, 45);
            this.cmbTipoCamion.Name = "cmbTipoCamion";
            this.cmbTipoCamion.Size = new System.Drawing.Size(107, 21);
            this.cmbTipoCamion.TabIndex = 23;
            // 
            // txtNumeroEjes
            // 
            this.txtNumeroEjes.Location = new System.Drawing.Point(177, 19);
            this.txtNumeroEjes.Name = "txtNumeroEjes";
            this.txtNumeroEjes.Size = new System.Drawing.Size(107, 20);
            this.txtNumeroEjes.TabIndex = 1;
            // 
            // lbOvertime
            // 
            this.lbOvertime.AutoSize = true;
            this.lbOvertime.Location = new System.Drawing.Point(19, 22);
            this.lbOvertime.Name = "lbOvertime";
            this.lbOvertime.Size = new System.Drawing.Size(82, 13);
            this.lbOvertime.TabIndex = 0;
            this.lbOvertime.Text = "Número de Ejes";
            // 
            // gbAutobus
            // 
            this.gbAutobus.Controls.Add(this.cmbTipoServicio);
            this.gbAutobus.Controls.Add(this.txtCantidadAsientos);
            this.gbAutobus.Controls.Add(this.lbCommission);
            this.gbAutobus.Controls.Add(this.lbAmountOfSales);
            this.gbAutobus.Location = new System.Drawing.Point(351, 112);
            this.gbAutobus.Name = "gbAutobus";
            this.gbAutobus.Size = new System.Drawing.Size(299, 80);
            this.gbAutobus.TabIndex = 11;
            this.gbAutobus.TabStop = false;
            this.gbAutobus.Text = "Autobus";
            // 
            // cmbTipoServicio
            // 
            this.cmbTipoServicio.FormattingEnabled = true;
            this.cmbTipoServicio.Location = new System.Drawing.Point(177, 48);
            this.cmbTipoServicio.Name = "cmbTipoServicio";
            this.cmbTipoServicio.Size = new System.Drawing.Size(107, 21);
            this.cmbTipoServicio.TabIndex = 22;
            // 
            // txtCantidadAsientos
            // 
            this.txtCantidadAsientos.Location = new System.Drawing.Point(177, 19);
            this.txtCantidadAsientos.Name = "txtCantidadAsientos";
            this.txtCantidadAsientos.Size = new System.Drawing.Size(107, 20);
            this.txtCantidadAsientos.TabIndex = 1;
            // 
            // lbCommission
            // 
            this.lbCommission.AutoSize = true;
            this.lbCommission.Location = new System.Drawing.Point(19, 48);
            this.lbCommission.Name = "lbCommission";
            this.lbCommission.Size = new System.Drawing.Size(84, 13);
            this.lbCommission.TabIndex = 2;
            this.lbCommission.Text = "Tipo de Servicio";
            // 
            // lbAmountOfSales
            // 
            this.lbAmountOfSales.AutoSize = true;
            this.lbAmountOfSales.Location = new System.Drawing.Point(19, 22);
            this.lbAmountOfSales.Name = "lbAmountOfSales";
            this.lbAmountOfSales.Size = new System.Drawing.Size(107, 13);
            this.lbAmountOfSales.TabIndex = 0;
            this.lbAmountOfSales.Text = "Cantidad de Asientos";
            // 
            // gbAutomovil
            // 
            this.gbAutomovil.Controls.Add(this.cmbTipoAuto);
            this.gbAutomovil.Controls.Add(this.txtNumeroPuertas);
            this.gbAutomovil.Controls.Add(this.lbAmountOfPiece);
            this.gbAutomovil.Controls.Add(this.lbPartsProduced);
            this.gbAutomovil.Location = new System.Drawing.Point(351, 26);
            this.gbAutomovil.Name = "gbAutomovil";
            this.gbAutomovil.Size = new System.Drawing.Size(299, 80);
            this.gbAutomovil.TabIndex = 10;
            this.gbAutomovil.TabStop = false;
            this.gbAutomovil.Text = "Automóvil";
            // 
            // cmbTipoAuto
            // 
            this.cmbTipoAuto.FormattingEnabled = true;
            this.cmbTipoAuto.Location = new System.Drawing.Point(177, 48);
            this.cmbTipoAuto.Name = "cmbTipoAuto";
            this.cmbTipoAuto.Size = new System.Drawing.Size(107, 21);
            this.cmbTipoAuto.TabIndex = 21;
            // 
            // txtNumeroPuertas
            // 
            this.txtNumeroPuertas.Location = new System.Drawing.Point(177, 19);
            this.txtNumeroPuertas.Name = "txtNumeroPuertas";
            this.txtNumeroPuertas.Size = new System.Drawing.Size(107, 20);
            this.txtNumeroPuertas.TabIndex = 1;
            // 
            // lbAmountOfPiece
            // 
            this.lbAmountOfPiece.AutoSize = true;
            this.lbAmountOfPiece.Location = new System.Drawing.Point(19, 48);
            this.lbAmountOfPiece.Name = "lbAmountOfPiece";
            this.lbAmountOfPiece.Size = new System.Drawing.Size(92, 13);
            this.lbAmountOfPiece.TabIndex = 2;
            this.lbAmountOfPiece.Text = "Tipo de Automóvil";
            // 
            // lbPartsProduced
            // 
            this.lbPartsProduced.AutoSize = true;
            this.lbPartsProduced.Location = new System.Drawing.Point(19, 22);
            this.lbPartsProduced.Name = "lbPartsProduced";
            this.lbPartsProduced.Size = new System.Drawing.Size(98, 13);
            this.lbPartsProduced.TabIndex = 0;
            this.lbPartsProduced.Text = "Número de Puertas";
            // 
            // lblTipoVehiculo
            // 
            this.lblTipoVehiculo.AutoSize = true;
            this.lblTipoVehiculo.Location = new System.Drawing.Point(18, 29);
            this.lblTipoVehiculo.Name = "lblTipoVehiculo";
            this.lblTipoVehiculo.Size = new System.Drawing.Size(89, 13);
            this.lblTipoVehiculo.TabIndex = 0;
            this.lblTipoVehiculo.Text = "Tipo de Vehículo";
            // 
            // cmbTypeOfVehicle
            // 
            this.cmbTypeOfVehicle.FormattingEnabled = true;
            this.cmbTypeOfVehicle.Location = new System.Drawing.Point(120, 26);
            this.cmbTypeOfVehicle.Name = "cmbTypeOfVehicle";
            this.cmbTypeOfVehicle.Size = new System.Drawing.Size(204, 21);
            this.cmbTypeOfVehicle.TabIndex = 1;
            this.cmbTypeOfVehicle.SelectedIndexChanged += new System.EventHandler(this.cmbTypeOfVehicle_SelectedIndexChanged);
            this.cmbTypeOfVehicle.SelectedValueChanged += new System.EventHandler(this.cmbTypeOfVehicle_SelectedValueChanged);
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Location = new System.Drawing.Point(18, 134);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(31, 13);
            this.lblColor.TabIndex = 8;
            this.lblColor.Text = "Color";
            // 
            // lblMarca
            // 
            this.lblMarca.AutoSize = true;
            this.lblMarca.Location = new System.Drawing.Point(18, 108);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(37, 13);
            this.lblMarca.TabIndex = 6;
            this.lblMarca.Text = "Marca";
            // 
            // txtAñoModelo
            // 
            this.txtAñoModelo.Location = new System.Drawing.Point(120, 79);
            this.txtAñoModelo.Name = "txtAñoModelo";
            this.txtAñoModelo.Size = new System.Drawing.Size(204, 20);
            this.txtAñoModelo.TabIndex = 5;
            // 
            // txtPlaca
            // 
            this.txtPlaca.Location = new System.Drawing.Point(120, 53);
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.Size = new System.Drawing.Size(204, 20);
            this.txtPlaca.TabIndex = 3;
            // 
            // lblAñoModelo
            // 
            this.lblAñoModelo.AutoSize = true;
            this.lblAñoModelo.Location = new System.Drawing.Point(18, 82);
            this.lblAñoModelo.Name = "lblAñoModelo";
            this.lblAñoModelo.Size = new System.Drawing.Size(64, 13);
            this.lblAñoModelo.TabIndex = 4;
            this.lblAñoModelo.Text = "Año Modelo";
            // 
            // lblPlaca
            // 
            this.lblPlaca.AutoSize = true;
            this.lblPlaca.Location = new System.Drawing.Point(18, 56);
            this.lblPlaca.Name = "lblPlaca";
            this.lblPlaca.Size = new System.Drawing.Size(34, 13);
            this.lblPlaca.TabIndex = 2;
            this.lblPlaca.Text = "Placa";
            // 
            // gbResults
            // 
            this.gbResults.Controls.Add(this.txtSeguro);
            this.gbResults.Controls.Add(this.lbNetEarnings);
            this.gbResults.Location = new System.Drawing.Point(12, 390);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(666, 70);
            this.gbResults.TabIndex = 2;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Resultado";
            // 
            // txtSeguro
            // 
            this.txtSeguro.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeguro.Location = new System.Drawing.Point(183, 28);
            this.txtSeguro.Name = "txtSeguro";
            this.txtSeguro.ReadOnly = true;
            this.txtSeguro.Size = new System.Drawing.Size(452, 35);
            this.txtSeguro.TabIndex = 1;
            // 
            // lbNetEarnings
            // 
            this.lbNetEarnings.AutoSize = true;
            this.lbNetEarnings.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNetEarnings.Location = new System.Drawing.Point(22, 34);
            this.lbNetEarnings.Name = "lbNetEarnings";
            this.lbNetEarnings.Size = new System.Drawing.Size(162, 25);
            this.lbNetEarnings.TabIndex = 0;
            this.lbNetEarnings.Text = "Seguro a Pagar";
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 474);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.gbAboutEmployee);
            this.Controls.Add(this.panelHeader);
            this.Name = "EmployeeForm";
            this.Text = "Calculadora de Salario";
            this.Load += new System.EventHandler(this.EmployeeForm_Load);
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.gbAboutEmployee.ResumeLayout(false);
            this.gbAboutEmployee.PerformLayout();
            this.gbCamion.ResumeLayout(false);
            this.gbCamion.PerformLayout();
            this.gbAutobus.ResumeLayout(false);
            this.gbAutobus.PerformLayout();
            this.gbAutomovil.ResumeLayout(false);
            this.gbAutomovil.PerformLayout();
            this.gbResults.ResumeLayout(false);
            this.gbResults.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbFormTitle;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.GroupBox gbAboutEmployee;
        private System.Windows.Forms.GroupBox gbAutomovil;
        private System.Windows.Forms.TextBox txtNumeroPuertas;
        private System.Windows.Forms.Label lbAmountOfPiece;
        private System.Windows.Forms.Label lbPartsProduced;
        private System.Windows.Forms.Label lblTipoVehiculo;
        private System.Windows.Forms.ComboBox cmbTypeOfVehicle;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.TextBox txtAñoModelo;
        private System.Windows.Forms.TextBox txtPlaca;
        private System.Windows.Forms.Label lblAñoModelo;
        private System.Windows.Forms.Label lblPlaca;
        private System.Windows.Forms.Button btCalculate;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.GroupBox gbCamion;
        private System.Windows.Forms.TextBox txtNumeroEjes;
        private System.Windows.Forms.Label lbOvertime;
        private System.Windows.Forms.GroupBox gbAutobus;
        private System.Windows.Forms.TextBox txtCantidadAsientos;
        private System.Windows.Forms.Label lbCommission;
        private System.Windows.Forms.Label lbAmountOfSales;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.TextBox txtSeguro;
        private System.Windows.Forms.Label lbNetEarnings;
        private System.Windows.Forms.Label lblValorFiscal;
        private System.Windows.Forms.Label lblCapacidad;
        private System.Windows.Forms.TextBox txtValorFiscal;
        private System.Windows.Forms.TextBox txtCapacidad;
        private System.Windows.Forms.ComboBox cmbMarca;
        private System.Windows.Forms.ComboBox cmbColor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbTipoCamion;
        private System.Windows.Forms.ComboBox cmbTipoServicio;
        private System.Windows.Forms.ComboBox cmbTipoAuto;
    }
}